#!/usr/bin/env ruby

require 'fileutils'
require 'json'
require 'tempfile'

require 'dotenv'
Dotenv.load

def check_variables(*variables)
  variables.each do |variable|
    raise "You need to define #{variable} variable" if !ENV[variable] || ENV[variable].empty?
  end
end

class Deployer
  attr_reader :gitlab_group_name

  def initialize(gitlab_group_name)
    @gitlab_group_name = gitlab_group_name
  end

  def start_marathon_lb
    rewrite = {
        'GITLAB_GROUP_NAME' => gitlab_group_name,
        'MARATHON_LB_INSTANCE_NUMBERS' => ENV['MARATHON_LB_INSTANCE_NUMBERS']
    }
    add_service_with_rewrite('marathon-lb.service.json', rewrite) unless check_service("/#{gitlab_group_name}/marathon-lb")
    marathon_hosts = get_marathon_lb_addresses.join("\n- ")

    puts <<~OUTPUT

    marathon-lb is running on hosts:
    - #{marathon_hosts}

    Please find the public IPs of these hosts and add following DNS records:

    #{ENV['DOMAIN']} IN A your_public_ip
    ; When using more than one node for marathon-lb
    ; repeat the above line multiple times for each
    ; public IP

    *.#{ENV['DOMAIN']} IN CNAME #{ENV['DOMAIN']}.

    OUTPUT

    ask_yes_no('Do you want to continue?')
  end

  def start_letsencrypt_dcos
    rewrite = {
        'GITLAB_GROUP_NAME' => gitlab_group_name,
        'DOMAIN' => ENV['DOMAIN'],
        'LE_EMAIL' => ENV['LE_EMAIL']
    }
    add_service_with_rewrite('letsencrypt-dcos.service.json', rewrite) unless check_service("/#{gitlab_group_name}/letsencrypt-dcos")

    puts <<~OUTPUT

    At this moment we should wait until letsencrypt-dcos is working.

    Please go to your DC/OS dashboard and check the status of `/#{gitlab_group_name}/letsencrypt-dcos`
    service. It will be ready when `Deploying` will be changed tu `Running`.

    OUTPUT
    ask_yes_no('Is letsencrypt-dcos service in `Running` state?')
  end

  def start_gitlab
    rewrite = {
        'GITLAB_GROUP_NAME' => gitlab_group_name,
        'DOMAIN' => ENV['DOMAIN'],
        'GITLAB_AGENT_PRIVATE_IP' => ENV['GITLAB_AGENT_PRIVATE_IP']
    }
    on_rewriten_file('gitlab-omnibus-config.source', rewrite) do |temp_omnibus_config_file|
      omnibus_config = File.read(temp_omnibus_config_file).split("\n").join(' ')
      rewrite_with_omnibus_config = rewrite.merge({'GITLAB_OMNIBUS_CONFIG' => omnibus_config})

      add_service_with_rewrite('gitlab.service.json', rewrite_with_omnibus_config) unless check_service("/#{gitlab_group_name}/gitlab")
    end

    puts <<~OUTPUT

    Now we need to wait a while fur GitLab to be started.

    Please go to your DC/OS dashboard and check the status of `/#{gitlab_group_name}/gitlab` service.
    It will be ready when `Deploying` will be changed tu `Running`.

    After service will be started we need to set up the environment and get
    Registration token for Runners. Please follow these steps:

    1. Go to https://gitlab.#{ENV['DOMAIN']}/ and sign in to application.
       At first time you will be asked to create a password. Chose your
       own, repeate it and sign in into GitLab (using 'root' as the username
       and your password).

    OUTPUT
    ask_yes_no('Does GitLab work properly?')

    puts <<~OUTPUT

    2. Go to https://mattermost.#{ENV['DOMAIN']}/ and create an account using
       "GitLab Single-Sign-On" button. After creating the account you will
       be asked to create a team.

    OUTPUT
    ask_yes_no('Does Mattermost work properly?')

    puts <<~OUTPUT

    3. Go to https://gitlab.#{ENV['DOMAIN']}/admin/runners and copy the registration
       token. It will be required in a moment to register GitLab Runners.

    OUTPUT
    ask_yes_no('Do you have Registration token for GitLab Runner?')
    @registration_token = read_from_user('Please provide the Runner Registration token')
  end

  def start_gitlab_runners
    raise "Runners Registration token is missing" if registration_token.empty?

    rewrite = {
        'GITLAB_GROUP_NAME' => gitlab_group_name,
        'DOMAIN' => ENV['DOMAIN'],
        'REGISTRATION_TOKEN' => registration_token
    }

    add_service_with_rewrite('gitlab-runner-docker.service.json', rewrite) unless check_service("/#{gitlab_group_name}/gitlab-runner-docker")
    add_service_with_rewrite('gitlab-runner-shell.service.json', rewrite) unless check_service("/#{gitlab_group_name}/gitlab-runner-shell")
  end

  def create_demo_group
    rewrite = {
        'GROUP_NAME' => ENV['GROUP_NAME']
    }

    create_group_with_rewrite('gitlab-i2p-dcos-demo.group.json', rewrite) unless check_group("/#{ENV['GROUP_NAME']}")
  end

  def create_gitlab_group
    rewrite = {
        'GITLAB_GROUP_NAME' => gitlab_group_name
    }

    create_group_with_rewrite('gitlab-i2p-dcos-demo-environment.group.json', rewrite) unless check_group("/#{gitlab_group_name}")
  end

  private

  def registration_token
    @registration_token ||= ''
  end

  def ask_yes_no(question)
    while true
      puts "#{question} [yes/no]"
      case gets.chomp.downcase
        when 'yes'
          return true
        when 'no'
          return false
      end
    end
  end

  def read_from_user(message)
    print "#{message} ~> "
    return gets.chomp
  end

  def dcos(args = [], opts = {})
    default_opts = {
        exception_on_failure: true
    }
    opts = default_opts.merge(opts)

    args.unshift('dcos')
    cmd = args.join(" ")

    puts cmd if ENV['DEBUG']
    unless system(cmd)
      raise 'Error while executing dcos command' if opts[:exception_on_failure]
      return false
    end
    true
  end

  def dcos_app(args = [], opts = {})
    dcos args.unshift(:app).unshift(:marathon), opts
  end

  def dcos_group(args = [], opts = {})
    dcos args.unshift(:group).unshift(:marathon), opts
  end

  def add_service(service_file)
    dcos_app [:add, service_file]
  end

  def check_service(service_id)
    dcos_app [:show, service_id, '>/dev/null', '2>&1'], { exception_on_failure: false }
  end

  def create_group(group_file)
    dcos_group [:add, group_file]
  end

  def check_group(group_id)
    dcos_group [:show, group_id, '>/dev/null', '2>&1'], { exception_on_failure: false }
  end

  def on_rewriten_file(file, rewrite = {})
    tempfile = Tempfile.new(file)
    configuration = File.read(file)
    rewrite.each do |pattern, replacement|
      configuration.gsub!(/__#{pattern}__/, replacement)
    end
    tempfile.write(configuration)
    tempfile.close

    yield tempfile.path
  ensure
    tempfile.close
    tempfile.unlink
  end

  def add_service_with_rewrite(service_file, rewrite = {})
    on_rewriten_file(service_file, rewrite) do |tempfile|
      add_service(tempfile)
    end
  end

  def create_group_with_rewrite(group_file, rewrite = {})
    on_rewriten_file(group_file, rewrite) do |tempfile|
      create_group(tempfile)
    end
  end

  def get_marathon_lb_addresses
    ips = []

    service_data_file = 'marathon_lb.service.data'
    dcos_app [:show, "/#{gitlab_group_name}/marathon-lb", ">#{service_data_file}"]
    data = JSON.parse(File.read(service_data_file))

    ips = data['tasks'].map{ |task| task['host'] }
  ensure
    FileUtils.rm(service_data_file, force: true)
    ips
  end
end

begin
  check_variables('GITLAB_GROUP_NAME', 'DOMAIN',
                  'LE_EMAIL', 'GITLAB_AGENT_PRIVATE_IP',
                  'GROUP_NAME', 'MARATHON_LB_INSTANCE_NUMBERS')

  d = Deployer.new(ENV['GITLAB_GROUP_NAME'])
  d.create_gitlab_group
  d.create_demo_group
  exit 0 unless d.start_marathon_lb
  d.start_letsencrypt_dcos
  d.start_gitlab
  d.start_gitlab_runners
rescue => e
  puts "An errror occured:\n\n  #{e}"
end
