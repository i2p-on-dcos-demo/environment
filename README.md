# GitLab I2P on DC/OS demo - environment configuration

Configuration and setup script for DC/OS based GitLab + Mattermost
environment for Idea To Production on DC/OS demo.

## Author

Tomasz Maczukin, 2017, GitLab

## License

MIT
